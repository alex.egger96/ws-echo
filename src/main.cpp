#include <iostream>

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

namespace ws = websocketpp;

void onMessage(ws::server<ws::config::asio> *s, ws::connection_hdl handle, ws::server<ws::config::asio>::message_ptr msgPtr) {
  std::cout << msgPtr->get_payload() << std::endl;

  if(msgPtr->get_payload() == "stop-listening") {
    s->stop_listening();
    return;
  }

  try {
    s->send(handle, msgPtr->get_payload(), msgPtr->get_opcode());
  } catch (ws::exception const &e) {
    std::cout << e.what() << std::endl;
  }

}

int main(void) {
  ws::server<ws::config::asio> echoServer;
  try {
    echoServer.set_access_channels(ws::log::alevel::all);

    echoServer.init_asio();

    echoServer.set_message_handler(ws::lib::bind(&onMessage, &echoServer, ws::lib::placeholders::_1, ws::lib::placeholders::_2));
    echoServer.listen(9003);
    echoServer.start_accept();
    echoServer.run();
  } catch (ws::exception const &e) {
    std::cout << e.what() << std::endl;
  }

}
